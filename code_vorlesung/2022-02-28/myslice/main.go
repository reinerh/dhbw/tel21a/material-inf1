package main

import (
	"fmt"
)

type MySlice struct {
	data     *[5]int
	size     int
	max_size int
}

func Append(slice MySlice, value int) MySlice {
	/* Dieser Code funktioniert nicht, soll aber die Idee zeigen, wie eine Slice bei Bedarf vergrößert wird.
	   if slice.size == slice.max_size {
	     newdata := new([2*slice.max_size]int)
	     for i,v := range slice.data {
	       newdata[i] = v
	     }
	     slice.data = newdata
	     slice.max_size *= 2
	   }
	*/

	slice.data[slice.size] = value
	slice.size++
	return slice
}

func MakeMySlice() MySlice {
	return MySlice{new([5]int), 0, 5}
}

func main() {

	s1 := MakeMySlice()
	s1 = Append(s1, 3)
	s1 = Append(s1, 17)
	s1 = Append(s1, 47)
	s1 = Append(s1, 38)
	s1 = Append(s1, 23)
	s1 = Append(s1, 55)

	fmt.Println(s1)
	fmt.Println(s1.data)
}
