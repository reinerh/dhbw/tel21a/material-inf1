package main

import (
	"fmt"
)

func Beispiel3() {
	// Pointer definieren, ohne eine Variable anzugeben, auf die er zeigt:
	intptr := new(int) // "Auf dem Heap"

	fmt.Println(intptr)
	fmt.Println(*intptr)
}
