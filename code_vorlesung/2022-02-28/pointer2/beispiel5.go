package main

import (
	"fmt"
)

func Beispiel5() {
	p := MakePointer2()
	UseAndChangePointer2(*p)
	ShowPointer2(p)
}

// Liefert einen Int-Pointer.
func MakePointer2() *int {
	return new(int)
}

// Den Pointer benutzen und verändern.
func UseAndChangePointer2(intvalue int) {
	fmt.Println(intvalue)
	intvalue += 3
}

// Gibt den Pointer aus.
func ShowPointer2(ptr *int) {
	fmt.Println(*ptr)
}
