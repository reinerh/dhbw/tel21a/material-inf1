package main

import (
	"fmt"
)

type Vektor2D struct {
	x, y int
}

func (v *Vektor2D) add(other Vektor2D) {
	v.x += other.x
	v.y += other.y
}

func sum(v1, v2 Vektor2D) Vektor2D {
	return Vektor2D{v1.x + v2.x, v1.y + v2.y}
}

func Beispiel6() {
	v1 := Vektor2D{3, 2}
	v2 := Vektor2D{5, -3}

	fmt.Println(v1)
	fmt.Println(v2)
	fmt.Println(sum(v1, v2))

	v1.add(v2)
	fmt.Println(v1) // Soll auch (8,-1) ausgeben.

	// Noch 10 mal addieren.
	for i := 0; i < 10; i++ {
		v1.add(v2)
	}
	fmt.Println(v1) // Soll auch (8,-1) ausgeben.

}
