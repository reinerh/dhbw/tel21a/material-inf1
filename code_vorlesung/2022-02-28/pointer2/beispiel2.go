package main

import (
	"fmt"
)

func Beispiel2() {
	i := 42
	j := 38

	ptr1 := &i
	ptr2 := &j

	// ptr1 soll statt auf i nun auf das zeigen, worauf ptr2 zeigt.
	// Also ptr1 = Adresse von j.
	ptr1 = ptr2

	j = 77

	// Durch die Zuwiesung oben zeigt ptr1 nun auch auf j.
	fmt.Println("1: ", i)
	fmt.Println("2: ", j)

	fmt.Println("3: ", ptr1)
	fmt.Println("4: ", ptr2)

	fmt.Println("5: ", *ptr1)
	fmt.Println("6: ", *ptr2)
}
