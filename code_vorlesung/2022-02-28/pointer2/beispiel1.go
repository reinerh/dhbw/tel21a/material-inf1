package main

import (
	"fmt"
)

func Beispiel1() {
	// Eine int-Variable definieren.
	var i int

	// Einen Pointer auf i definieren.
	ptr_i := &i

	// Pointer auf den Pointer auf i.
	ptr_ptr_i := &ptr_i

	// Die Zahl i verändern (alle folgenden sind gleichwertig).
	// i = 42
	// *ptr_i = 77
	**ptr_ptr_i = 23

	// Gibt den Wert von i aus.
	fmt.Println(i)
	// Gibt die Adresse aus, an der i steht.
	fmt.Println(ptr_i)
	// Gibt die Adresse aus, an der ptr_i steht.
	fmt.Println(ptr_ptr_i)

	// Gibt i mittels ptr_i aus.
	fmt.Println(*ptr_i)

	// Gibt ptr_i (also die Adresse von i) mittels ptr_ptr_i aus.
	fmt.Println(*ptr_ptr_i)

	// Gibt i mittels ptr_ptr_i aus.
	fmt.Println(**ptr_ptr_i)

	// Die Typen der drei Variablen ausgeben.
	fmt.Printf("%T\n", i)
	fmt.Printf("%T\n", ptr_i)
	fmt.Printf("%T\n", ptr_ptr_i)
}
