package main

import (
	"fmt"
)

func Beispiel4() {
	p := MakePointer()
	UseAndChangePointer(p)
	ShowPointer(p)
}

// Liefert einen Int-Pointer.
func MakePointer() *int {
	return new(int)
}

// Den Pointer benutzen und verändern.
func UseAndChangePointer(intptr *int) {
	fmt.Println(intptr)
	fmt.Println(*intptr)
	*intptr += 3
}

// Gibt den Pointer aus.
func ShowPointer(ptr *int) {
	fmt.Println(*ptr)
}
