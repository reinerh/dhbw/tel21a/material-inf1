package main

import "fmt"

type Foo struct {
	x int // "Attribut" oder "Daten-Member" von Foo.
}

func PlusZwei(i int) int {
	return i + 2
}

func (k Foo) XPlus2() int {
	return k.x + 2
}

type MyInt int

func (m MyInt) Minus2() int {
	return int(m) - 2
}

func main() {
	f := Foo{42}

	fmt.Println(f.x)

	fmt.Println(PlusZwei(f.x))

	fmt.Println(f.XPlus2())

	i := MyInt(23)
	fmt.Println(i.Minus2())

	l1 := []int{10, 20, 30, 40, 50}

	fmt.Println(l1[:3])

}
