package main

import (
	"fmt"
)

// Liefert true, falls x < y
func lessThan(x, y int) bool {
	return x < y
}

// Liefert true, falls x > y
func greaterThan(x, y int) bool {
	return x > y
}

// Erwartet eine Liste und liefert true,
// falls diese sortiert ist.
// Das Sortierkriterium wird durch den "Komparator" correctOrder festgelegt.
func isSorted(list []int, correctOrder func(int, int) bool) bool {
	for i := range list[:len(list)-1] {
		// Falls das hier true wird, ist das der Beweis,
		// dass die Liste nicht passend sortiert ist.
		if !correctOrder(list[i], list[i+1]) {
			return false
		}
	}
	return true
}

func isSortedAscending(list []int) bool {
	return isSorted(list, lessThan) // Sortierprüfung mittels der Funktion lessThan
}
func isSortedDescending(list []int) bool {
	return isSorted(list, greaterThan) // Sortierprüfung mittels greaterThan
}

// Version einer isSorted(), die nicht von der Sortierreihenfolge abhängt.
/*
func isSorted(list []int) bool {
  return isSortedAscending(list) || isSortedDescending(list)

Alternative, wenn man die beiden Funktionen oben nicht nutzen kann oder will.
  ascending := list[0] < list[1]

  for i := range list[:len(list)-1] {
    if ascending && list[i] > list[i+1] {
      return false
    }
    if !ascending && list[i] < list[i+1] {
      return false
    }
  }
  return true
}
*/

func main() {

	l1 := []int{55, 42, 35, 3, 1}
	l2 := []int{1, 3, 35, 42, 55}
	l3 := []int{3, 1, 35, 55, 42}

	fmt.Println(isSortedAscending(l1))
	fmt.Println(isSortedAscending(l2))
	fmt.Println(isSortedAscending(l3))

	fmt.Println(isSortedDescending(l1))
	fmt.Println(isSortedDescending(l2))
	fmt.Println(isSortedDescending(l3))

	fmt.Println(isSorted(l1, greaterThan))
}
