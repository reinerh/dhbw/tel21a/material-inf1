# Code aus der Vorlesung

In diesem Ordner wird der Code aus den Vorlesungen gesammelt, genau oder sehr ähnlich
wie er dort entstanden ist. Dieser Code wird nach Möglichkeit sehr schnell nach den
Vorlesungen aktualisiert und wird dann im Nachgang aufbereitet. Aufbereitete Formen
können dann an anderen Stellen im Repo auftauchen, z.B. im Ordner `beispiele_vorlesung`.