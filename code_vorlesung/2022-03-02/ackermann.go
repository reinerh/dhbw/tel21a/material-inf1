package main

import (
	"fmt"
)

func main() {
	fmt.Println(ack(4, 4))
}

func ack(m, n int) int {
	if m == 0 {
		return n + 1
	}
	if n == 0 {
		return ack(m-1, 1)
	}
	return ack(m-1, ack(m, n-1))

}
