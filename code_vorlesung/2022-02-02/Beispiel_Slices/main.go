package main

import (
	"fmt"
)

func main() {

	// Int-Slice erzeugen
	s1 := []int{10, 20, 30, 40, 50}

	fmt.Println(s1)

	// Weitere Slice aus der ersten erzeugen:
	s2 := s1[1:4]

	fmt.Println(s2)

	// s2 an der mittleren Stelle verändern:
	s2[1] = 9999

	fmt.Println(s1)
	fmt.Println(s2)

	s3 := s2[1:]
	fmt.Println(s3)

	s1 = append(s1, 1000, 2000, 3000, 4000)
	s1[1] = 42

	fmt.Println(s1)
	fmt.Println(s2)

}
