package main

import (
	"fmt"
)

/*
// Liefert die Summe der Zahlen von 1 bis n.
// Version 1
func sum(n int) int {
  result := 0
  for n!=0 {
    result = result + n
    n = n - 1
  }
	return result
}
*/

/*
// Liefert die Summe der Zahlen von 1 bis n.
// Version 2
func sum(n int) int {
  result := 0
  for i:=0; i<=n; i+=1 {
    result += i
  }
	return result
}
*/

// Version 3

// Hilfsfunktion, die eine Liste mit den Zahlen von 1 bis n mit Schrittweite step liefert.
func numbers(n int, start int, step int) []int {
	result := []int{} // Leere Liste erzeugen.
	for i := start; i <= n; i += step {
		result = append(result, i)
	}
	return result
}

// Hilfsfunktion, die eine Liste von Zahlen erwartet und die deren Summe liefert.
func sumList(list []int) int {
	result := 0
	for _, value := range list {
		result += value
	}
	return result
}

// Implementierung von sum() mit Hilfe von numbers() und sumList().
func sum(n int) int {
	return sumList(numbers(n, 1, 1))
}

// Eine Funktion, die die Summe aller geraden Zahlen zwischen 1 und n liefert.
func sumEven(n int) int {
	return sumList(numbers(n, 2, 2))
}

func main() {
	fmt.Println(sum(10))
	fmt.Println(sumEven(10))
	//fmt.Println(numbers(10))
	//fmt.Println(sumList([]int{1,2,3,4,5,6,7,8,9,10}))
}

// Aufgaben:
// Eine Funktion, die die Summe aller durch 5 teilbaren Zahlen bis n liefert.

// Eine Funktion, die die Summe aller durch 17 teilbaren Zahlen bis n liefert.

// Eine Funktion, die die Summe aller Primzahlen unter n liefert.
// Hinweis: Die numbers()-Funktion reicht hier nicht mehr, aber sumList() kann wiederverwendet werden.
