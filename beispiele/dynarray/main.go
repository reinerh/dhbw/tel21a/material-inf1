package main

import (
	"fmt"
)

type DynamicArray struct {
	data     []int
	size     int
	max_size int
}

func Append(array DynamicArray, value int) DynamicArray {

	if array.size == array.max_size {
		backup := make([]int, array.size)
		copy(backup, array.data)
		array.data = make([]int, 2*array.max_size)
		copy(array.data, backup)
		array.max_size *= 2
	}

	array.data[array.size] = value
	array.size++
	return array
}

func MakeDynamicArray() DynamicArray {
	return DynamicArray{make([]int, 5), 0, 5}
}

func main() {

	s1 := MakeDynamicArray()
	s1 = Append(s1, 3)
	s1 = Append(s1, 17)
	s1 = Append(s1, 47)
	s1 = Append(s1, 38)
	s1 = Append(s1, 23)
	s1 = Append(s1, 55)

	fmt.Println(s1)
	fmt.Println(s1.data)
}
