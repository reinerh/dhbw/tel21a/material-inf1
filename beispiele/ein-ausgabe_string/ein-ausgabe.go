package main

import (
	"fmt"
)

func main() {
	// Aufforderung, einen Text einzugeben.
	// Dieses Mal nur "Print", keine neue Zeile.
	fmt.Print("Bitte eine Zahl eingeben: ")

	// Eine Variable vom Typ string deklarieren. string steht für "Zeichenkette".
	// Die Variable hier ist dafür gedacht, die Eingabe des Benutzers zu speichern.
	var input string

	// Eine Benutzereingabe lesen.
	// Scanln() liest bis zum nächsten Whitespace.
	// Die Eingabe wird in die Variable input eingelesen.
	fmt.Scanln(&input)

	// Drei verschiedene Methoden, die Variable wieder auszugeben.
	fmt.Println("Sie haben eingegeben: " + input)
	fmt.Println("Sie haben eingegeben:", input)
	fmt.Printf("Sie haben %v eingegeben.\n", input)
}
