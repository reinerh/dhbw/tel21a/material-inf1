package main

import "fmt"

// Gibt "foo" aus.
func printFoo() {
	fmt.Println("foo")
}

// Gibt "bar" aus.
func printBar() {
	fmt.Println("bar")
}

// Erwartet eine Funktion und ruft sie auf.
func call(f func()) {
	f()
}

func main() {
	// Beispiel 1: Die Funktionen printFoo und printBar jeweils einmal indirekt aufrufen.
	call(printFoo)
	call(printBar)

	// Beispiel 2: Eine Funktion hier lokal definieren und mittels call aufrufen.
	f := func() { fmt.Println("f aufgerufen.") }
	call(f)

	// Beispiel 3: Eine Funktion lokal definieren, die einen Wert erwartet und einen zurückliefert.
	plus3 := func(i int) int { return i + 3 }
	fmt.Println(plus3(5))
	fmt.Println(plus3(2))

	// Beispiel 4: Eine Funktion lokal definieren, die eine lokale Variable einfängt und verändert.
	r := 0
	addToR := func(i int) { r += i }
	addToR(4)
	fmt.Println(r) // Gibt 4 aus.
	addToR(-2)
	fmt.Println(r) // Gibt 2 aus.

	// Beispiel 5: Mittels addToR() alle Elemente einer Liste aufsummieren.
	r = 0
	l1 := []int{1, 2, 3, 4, 5}
	for _, v := range l1 {
		addToR(v)
	}
	fmt.Println(r) // Gibt 15 aus.
}
