# Übungsaufgaben zu Listen

Dieses Verzeichnis enthält eine Reihe von Übungsaufgaben zu Listen.
Die Aufgaben bauen teilweise aufeinander auf, meist gibt es eine Vorbereitungsaufgabe,
die eine Liste erzeugt und dann eine oder mehrere darauf aufbauende Aufgaben, in denen
diese Liste dann benutzt werden muss.